\documentclass[xcolor=dvipsnames,10pt]{beamer}

\makeatother
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.4\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.6\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle\hspace*{3em}
    \insertframenumber{} / \inserttotalframenumber\hspace*{1ex}
  \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatletter
\setbeamertemplate{navigation symbols}{}
\usetheme{Boadilla}
\usepackage{xfrac}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,chains,positioning,fit,calc}
\usepackage{cleveref}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{amsfonts,amssymb,latexsym}
\usepackage{amsmath}
\usepackage{upgreek}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{multimedia}
\usepackage{media9}
\usepackage{xmpmulti}
\usepackage{animate}
\usepackage{appendixnumberbeamer}

\newcommand{\backupbegin}{
   \newcounter{finalframe}
   \setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
   \setcounter{framenumber}{\value{finalframe}}
}

\usepackage[utf8]{inputenc}

\defbeamertemplate{subsection in toc}{subsections numbered roman}{%
  \leavevmode
 {\romannumeral\inserttocsubsection}\par}
\defbeamertemplate{section in toc}{sections numbered roman}{%
  \leavevmode%
  \MakeUppercase{\romannumeral\inserttocsectionnumber}.\ %
  \inserttocsection\par}

\setbeamertemplate{section in toc}[sections numbered roman]
\setbeamertemplate{subsection in toc}[bullets]

\title[EMI 18]{Global sensitivity analysis for nonlinear aeroelastic vibrations of a cantilever}
\subtitle{with structural and aerodynamic nonlinearities$^*$}
\author[B.~Robinson \textit{et al.}]{B. Robinson \inst{1}, R. Sandhu \inst{1}, M. Khalil \inst{2}, D. Poirel \inst{3}, C. Pettit \inst{4} and A. Sarkar\inst{1}}
\institute[]{
  \inst{1}%
  Carleton University, Ottawa, ON, Canada
      \and
  \inst{2}%
  Sandia National Laboratories, Livermore, CA, USA    
      \and 
  \inst{3}%
  Royal Military College of Canada, Kingston, ON, Canada
    \and
    \inst{4}%
  United States Naval Academy, Annapolis, MD, USA
\\
\vspace{0.2in}
{\tiny\it
Sandia National Laboratories is a multimission laboratory managed and operated by National Technology and Engineering Solutions of Sandia, LLC., a wholly owned subsidiary of Honeywell International, Inc., for the U.S. Department of Energy's National Nuclear Security Administration under contract DE-NA-0003525.
}}
\date{June 1, 2018}


\begin{document}

\begin{frame}
\titlepage
\end{frame}


\begin{frame}
\frametitle{Outline}
\tableofcontents
\end{frame}

%\begin{frame}
%\begin{block}{Motivation}
%Unforced, self-sustained, Large Amplitude Oscillations (LAO) and Small Amplitude Oscillations (SAO) were observed for varying airspeeds during wind tunnel tests at the Royal Military College of Canada.
%\end{block}
%\begin{figure}
%\includegraphics[width=\linewidth]{figs/LAOSAO}
%\caption{LAO and SAO observed for a gradual decrease in airspeed, \textit{Jose Rocha da Costa [2016]}}
%\end{figure}
%\end{frame}

\section{Introduction}

\subsection{The Aeroelastic System}
\begin{frame}
\frametitle{Introduction}
%% Dominique's schematics
\begin{columns}
\column{0.5\textwidth}
\begin{block}{Wind Tunnel Apparatus}
\begin{description}[Support]
\item[Support] Aluminum support enables the rigid body base rotation
\item[Beam] Steel beam with a rectangular cross section provides the structural stiffness
\item[Wing] Foam and plastic provides the overall assembly with the NACA0012 airfoil profile.
\end{description}
\end{block}
\column{0.4\textwidth}
\begin{figure}
\includegraphics[width=0.75\linewidth]{figs/DPWing}
\caption{Schematic of the assembled wing apparatus}
\end{figure}
\end{columns}
\end{frame}

\subsection{Motivation}
\begin{frame}
\frametitle{Introduction}
\begin{block}{Motivation}
Unforced, self-sustained, Large Amplitude Oscillations (LAO) and Small Amplitude Oscillations (SAO) were observed during wind tunnel experiments performed at the Royal Military College of Canada.
\end{block}
\vspace{0.25cm}
\begin{columns}
\column{0.4\textwidth}
\centering
\animategraphics[loop,width=0.75\linewidth]{12}{figs/animate-}{0}{29}\\
Large Amplitude Oscillations
\column{0.4\textwidth}
\centering
\animategraphics[loop,width=0.75\linewidth]{12}{figs/sao-}{0}{37}\\
Small Amplitude Oscillations
\end{columns}
%%%%%
%\includemedia[
%     label=myVidPlayer,
%     width=6cm,height=6cm,
%     activate=pageopen,
%     addresource=figs/video1.mp4,
%     addresource=figs/video2.mp4,
%     flashvars={
%         source=figs/video1.mp4 %video to be shown at first
%        &autoPlay=true
%        &scaleMode=letterbox %useful, if videos are of different aspect ratio
%     }
%]{\includegraphics[width=0.5\linewidth]{figs/animate-0}}{VPlayer9.swf}
% 
%\mediabutton[mediacommand=myVidPlayer:setSource [(figs/video1.mp4)]]{Large Amplitude Oscillations}\\
%\mediabutton[mediacommand=myVidPlayer:setSource [(figs/video2.mp4)]]{Small Amplitude Oscillations}
%%%%%
%\includemedia[
%  addresource=figs/video1.mp4,
%  windowed=1024x768,
%  flashvars={
%    source=figs/video1.mp4
%   &autoPlay=true
%   &scaleMode=letterbox
%  }
%]{\includegraphics[width=1cm,height=1cm]{figs/animate-3}}{VPlayer.swf}
%%%%%
\end{frame}

\begin{frame}
\frametitle{Introduction}
\begin{block}{Motivation}
\begin{enumerate}[I]
\item We would like to perform state and parameter estimation on this system using:
\begin{enumerate}[i]
\item a nonlinear model for the structural vibrations,
\item a nonlinear model for the aerodynamic loads, and
\item sensor data from accelerometers and strain gauges installed on the wings used in experiments
\end{enumerate}
\item Facilitate this by deriving a Galerkin projection based reduced order model of the deterministic system
\item \textcolor{red}{Reduce the stochastic dimensionality of the problem using Global Sensitivity Analysis (GSA)}
\end{enumerate}
\end{block}
\end{frame}

\section{Nonlinear Equations of Motion}
\begin{frame}
\frametitle{Nonlinear Equations of Motion}
\begin{block}{Degrees of Freedom}
\begin{description}[align=right]
\item[$u(x,t)$] Axial displacement
\item[$v(x,t)$] Edgewise bending (in-plane)
\item[$w(x,t)$] Flapwise bending (out-of-plane)
\item[$\phi(x,t)$] Angle of twist about elastic axis
\item[$\theta(t)$] Base rotation about pitch axis
\end{description} 
\end{block}
\begin{figure}
\includegraphics[width=0.6\linewidth]{figs/DOFs}
\caption{Oblique view of the wing in its undeformed state and after deformation}
\end{figure}
\end{frame}

%\subsection{Source of Nonlinearities}
%\begin{frame}
%\frametitle{Introduction}
%\begin{block}{Souce of Nonlinearities}
%\begin{description}
%\item[Structural] Geometric nonlinearities due to large displacements
%\item[Aerodynamic] Nonlinear relationship between the drag, lift, and moment coefficients for large angles of attack
%\end{description}
%\end{block}
%\end{frame}

\subsection{Structural Equations}
\begin{frame}
\frametitle{Nonlinear Equations of Motion}
\begin{block}{Structural Equations}
\begin{enumerate}[I]
\item The derivation of the equations of motion follows the procedure outlined by \textit{Hodges and Dowell [1974]} for rotor blades \footnotemark
\item The kinematics of the motion were adapted to suit the current system
\end{enumerate}  
\end{block}
\begin{example}{}
\scriptsize{
\begin{align}
\bar{W}_w &= \Big\{  EA \Big(w''\int_0^S \Big(v''v'+w''w'\Big) dx -  \frac{1}{2}v'^2w'' - \frac{1}{2}w'^2w''\Big)   + \Big(EI_{\bar{z}} - EI_{\bar{y}}\Big) \Big(v''''\phi  + 2 v'''\phi' + v'' \phi'' \Big)\notag \\
& \qquad  + EI_{\bar{y}}\Big(w'''' \Big)  \Big\}  + \Big\{\bar{m} \ddot{w} + \bar{m}e \ddot{\phi}+ \textcolor{red}{\bar{m} \ddot{\theta}(v+e) - \bar{m} \dot{\theta}^2(e\phi + w) + 2\bar{m}\dot{\theta}(\dot{v})\Big\}} 
\\ \ \notag  \\
\textcolor{red}{\bar{W}_\theta } & \textcolor{red}{=I_\theta \ddot{\theta} + D_\theta \dot{\theta} + K_\theta \theta + \int_0^S  \Big\{\bar{I}_O (\ddot{\phi} + \ddot{\theta}) -\bar{m}\ddot{v}(e \phi+w) + \bar{m}\ddot{w}(v+e) + \bar{m}e\ddot{\phi}(v) + \bar{m}e\ddot{\theta}(v+ w\phi)} \notag \\
& \qquad  \textcolor{red}{+ \bar{m}\ddot{\theta}(w^2 +v^2)  + 2\bar{m}\dot{\theta}\big(\dot{v}(v+e)) + \dot{w}(e\phi +w) + e\dot{\phi}(w)\big)  \Big\}dx} 
\end{align}}%
\end{example}
\footnotetext[1]{Hodges, Dewey H., and E. H. Dowell. ``Nonlinear equations of motion for the elastic bending and torsion of twisted nonuniform rotor blades.'' (1974).}
\end{frame}

%\subsection{Galerkin Projection}
%\begin{frame}
%\frametitle{Galerkin Projection}
%\begin{block}{Separation of Variables}
%\begin{enumerate}[I]
%\item The displacements, $u(x,t)$, $v(x,t)$, $w(x,t)$, the angle of twist $\phi(x,t)$, and their respective partial derivatives vary along the span and in time. 
%\item The base pitch rotation, $\theta(t)$ is solely a function of time. 
%\end{enumerate}
%\end{block}
%\begin{example}
% Flapwise bending displacement $w(x,t)$\\
% \scriptsize{
% \begin{align*}
% w(x,t) = \sum_{j=1}^{\infty} \Psi_j(x) W_j(t) \approx \sum_{j=1}^{M} \Psi_j(x) W_j(t) \\
% \end{align*} }%
% where $\Psi_i(x)$ are a set of $M$ orthonormal functions which satisfy the essential and natural boundary conditions of a cantilever beam according to Euler-Bernoulli beam theory, and $W_j(t)$ are $M$ unknown temporal coordinates
%\end{example}
%\end{frame}
%
%\begin{frame}
%\frametitle{Galerkin Projection}
%\begin{block}{Separation of Variables}
%\begin{enumerate}[I]
%\item The equations are nondimensionalized
%\item Galerkin projection is employed to discretize the system
%\end{enumerate}
%\end{block}
%\ \\
%\begin{example}
%Discretizing $w(x,t)$ on the domain [0, 1]\\
% \scriptsize{
% \begin{align*}
%  \int_0^1 \sum_{i=1}^{M}\sum_{j=1}^{M} \Psi_i(\mathcal{X})\Psi_j(\mathcal{X}) \mathcal{W}_j(\mathcal{T}) d\mathcal{X} = \sum_{i=1}^{M}\sum_{j=1}^{M} C_{w}^{ij}\mathcal{W}_j(\mathcal{T})\\
% \end{align*}}%
% where the coefficient $C_{w}^{ij}$ is an $M \times M$ tensor, and $W_j(t)$ is an $M \times 1$ vector
%\end{example}
%\end{frame}
%
%\begin{frame}
%\frametitle{Galerkin Projection}
%
%\begin{example}{PDE Governing Flapwise Bending Becomes $M$ Coupled ODEs}
%\scriptsize{
%\begin{align*}
% &\sum_{i=1}^{M}\sum_{j=1}^{L}\sum_{k=1}^{L}\sum_{l=1}^{M} \Big(C_{w1}^{ijkl} - \frac{1}{2}C_{w3}^{ijkl}\Big)  \mathcal{V}_{j}\mathcal{V}_{k}\mathcal{W}_{l} + \sum_{i=1}^{M}\sum_{j=1}^{M}\sum_{k=1}^{M}\sum_{l=1}^{M}  \Big( C_{w2}^{ijkl} - \frac{1}{2}C_{w4}^{ijkl} \Big) \mathcal{W}_{j}\mathcal{W}_{k}\mathcal{W}_{l}  \\
%& \quad + \mathcal{K}_y \Big\{\sum_{i=1}^{M}\sum_{j=1}^{M} C_{w16}^{ij} \mathcal{W}_{j}\Big\} + \Big(\mathcal{K}_z - \mathcal{K}_y\Big) \Big\{\sum_{i=1}^{M}\sum_{j=1}^{L}\sum_{k=1}^{N}\Big(C_{w17}^{ijk}  + 2 C_{w18}^{ijk}  + C_{w19}^{ijk} \Big) \mathcal{V}_{j}\varphi_{k} \Big\}  \\
%& \quad+ \sum_{i=1}^{M}\sum_{j=1}^{M}C_{w20}^{ij} \Big( \ddot{\mathcal{W}}_{j} - \dot{\vartheta}^2 \ \mathcal{W}_{j} \Big)  + \sum_{i=1}^{M}\sum_{j=1}^{N} C_{w21}^{ij} \mathcal{E}\Big( \ddot{\varphi}_{j} - \dot{\vartheta}^2 \varphi_{j}   \Big) + \sum_{i=1}^{M}C_{w22}^{i}\mathcal{E} \ddot{\vartheta} \\
%& \quad+ \sum_{i=1}^{M}\sum_{j=1}^{L}C_{w23}^{ij} \Big(\ddot{\vartheta} V_{j} + 2\dot{\vartheta} \dot{\mathcal{V}}_{j} \Big)= \int_0^1 \sum_{i=1}^N \Psi_i(\mathcal{X}) \mathcal{L} d\mathcal{X}
%\end{align*}}%
%\end{example}
%\end{frame}

%\subsection{Structural Simulation}
%\begin{frame}
%\frametitle{Structural Free Vibration Results}
%\begin{block}{Undamped Free Vibration (no flow)}
%For the same set of initial conditions, $\theta = 30 ^{\circ}$, the results from the linear (red) and nonlinear (blue) simulations are plotted on the same figures.
%\end{block}
%\begin{alertblock}{}
%\vspace{-0.3cm}
%\scriptsize{
%\begin{align*}
%& \mathcal{K}_y \sum_{i=1}^{M}\sum_{j=1}^{M} C_{w16}^{ij} \mathcal{W}_{j} + \sum_{i=1}^{M}\sum_{j=1}^{M}C_{w20}^{ij} \ddot{\mathcal{W}}_{j}   + \sum_{i=1}^{M}\sum_{j=1}^{N} C_{w21}^{ij} \mathcal{E} \ddot{\varphi}_{j} + \sum_{i=1}^{M}C_{w22}^{i}\mathcal{E} \ddot{\vartheta} = 0
%\end{align*}}%
%\end{alertblock}
%\begin{block}{}
%\vspace{-0.3cm}
%\scriptsize{
%\begin{align*}
% &\sum_{i=1}^{M}\sum_{j=1}^{L}\sum_{k=1}^{L}\sum_{l=1}^{M} \Big(C_{w1}^{ijkl} - \frac{1}{2}C_{w3}^{ijkl}\Big)  \mathcal{V}_{j}\mathcal{V}_{k}\mathcal{W}_{l} + \sum_{i=1}^{M}\sum_{j=1}^{M}\sum_{k=1}^{M}\sum_{l=1}^{M}  \Big( C_{w2}^{ijkl} - \frac{1}{2}C_{w4}^{ijkl} \Big) \mathcal{W}_{j}\mathcal{W}_{k}\mathcal{W}_{l}  \\
%& \quad + \mathcal{K}_y \Big\{\sum_{i=1}^{M}\sum_{j=1}^{M} C_{w16}^{ij} \mathcal{W}_{j}\Big\} + \Big(\mathcal{K}_z - \mathcal{K}_y\Big) \Big\{\sum_{i=1}^{M}\sum_{j=1}^{L}\sum_{k=1}^{N}\Big(C_{w17}^{ijk}  + 2 C_{w18}^{ijk}  + C_{w19}^{ijk} \Big) \mathcal{V}_{j}\varphi_{k} \Big\}  \\
%& \quad+ \sum_{i=1}^{M}\sum_{j=1}^{M}C_{w20}^{ij} \Big( \ddot{\mathcal{W}}_{j} - \dot{\vartheta}^2 \ \mathcal{W}_{j} \Big)  + \sum_{i=1}^{M}\sum_{j=1}^{N} C_{w21}^{ij} \mathcal{E}\Big( \ddot{\varphi}_{j} - \dot{\vartheta}^2 \varphi_{j}   \Big) + \sum_{i=1}^{M}C_{w22}^{i}\mathcal{E} \ddot{\vartheta} \\
%& \quad+ \sum_{i=1}^{M}\sum_{j=1}^{L}C_{w23}^{ij} \Big(\ddot{\vartheta} V_{j} + 2\dot{\vartheta} \dot{\mathcal{V}}_{j} \Big)= 0
%\end{align*}}%
%\end{block}
%\end{frame}
%
%\begin{frame}
%Flapwise Bending Results ($w$)
%\begin{figure}
%    \centering
%    \begin{subfigure}[b]{0.28\textwidth}
%        \includegraphics[width=\textwidth]{figs/WTimeUFVZoom}
%        \caption{\scriptsize{Time Trace (1s)}}%
%    \end{subfigure}
%\qquad 
%    \begin{subfigure}[b]{0.28\textwidth}
%        \includegraphics[width=\textwidth]{figs/WTimeUFV}
%        \caption{\scriptsize{Time Trace (5s)}}%
%    \end{subfigure}
%    \qquad 
%    \begin{subfigure}[b]{0.28\textwidth}
%        \includegraphics[width=\textwidth]{figs/WPSDUFV}
%        \caption{\scriptsize{Power Spectral Density}}%
%    \end{subfigure}
%\end{figure}
%Rigid Body Base Rotation ($\theta$)
%\begin{figure}
%    \centering
%    \begin{subfigure}[b]{0.28\textwidth}
%        \includegraphics[width=\textwidth]{figs/TTimeUFVZoom}
%        \caption{\scriptsize{Time Trace (1s)}}%
%    \end{subfigure}
%\qquad 
%    \begin{subfigure}[b]{0.28\textwidth}
%        \includegraphics[width=\textwidth]{figs/TTimeUFV}
%        \caption{\scriptsize{Time Trace (5s)}}%
%    \end{subfigure}
%    \qquad 
%    \begin{subfigure}[b]{0.28\textwidth}
%        \includegraphics[width=\textwidth]{figs/TPSDUFV}
%        \caption{\scriptsize{Power Spectral Density}}%
%    \end{subfigure}
%\end{figure}
%\end{frame}

%\subsection{Aerodynamic Model}
%\begin{frame}
%\frametitle{Nonlinear Equations of Motion}
%\begin{block}{Aerodynamic Model}
%From thin airfoil theory we get
%
%\begin{equation}
%L = \frac{1}{2}\rho U^2 c S C_{L \alpha} 
%\end{equation}%
%{\footnotesize
%where $C_{L \alpha}$ is the lift coefficient as a function of the structural displacements
%}
%\begin{equation}
%M_{EA} = e_c L + \frac{1}{2}\rho U^2 c^2 S C_{M_{AC}} 
%\end{equation}%
%{\footnotesize
%where $C_{M_{AC}}$ is the moment coefficient about the aerodynamic center
%}
%\end{block}
%\end{frame}



%= \frac{1}{2}\rho U^2 c S 2 \pi \left((\phi + \theta) + \frac{\dot{w}}{U} + \frac{(\dot{\phi} +\dot{\theta}) (\frac{1}{2}c - e_{c})}{U} \right)
%= \frac{1}{2}\rho U^2 c^2 S \left(-\frac{\pi c}{8U}(\dot{\phi} +\dot{\theta})\right)

\subsection{Aeroelastic Simulation}
\begin{frame}
\frametitle{Aeroelastic Simulation}
Sample Flapwise Bending Tip Displacement, $w(1,t)$ (\textcolor{red}{linear} and \textcolor{blue}{nonlinear} models) 
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figs/WTimeLCOZoom}
        \caption{\scriptsize{Time Trace (1s)}}%
    \end{subfigure}
\qquad 
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figs/WTimeLCO}
        \caption{\scriptsize{Time Trace (5s)}}%
    \end{subfigure}
    \qquad 
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figs/WPSDLCO}
        \caption{\scriptsize{Power Spectral Density}}%
    \end{subfigure}
\end{figure}
Sample Rigid Body Base Rotation, $\theta(t)$ (\textcolor{red}{linear} and \textcolor{blue}{nonlinear} models) 
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figs/TTimeLCOZoom}
        \caption{\scriptsize{Time Trace (1s)}}%
    \end{subfigure}
\qquad 
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figs/TTimeLCO}
        \caption{\scriptsize{Time Trace (5s)}}%
    \end{subfigure}
    \qquad 
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figs/TPSDLCO}
        \caption{\scriptsize{Power Spectral Density}}%
    \end{subfigure}
\end{figure}
\end{frame}

\section{Global Sensitivity Analysis}
\begin{frame}
\frametitle{Global Sensitivity Analysis}
\begin{block}{Purpose of GSA}
We would like to assess how much of the uncertainty in the system's output can be apportioned to the uncertainty in the model inputs.
\\ \ \\
Seeing the effect of the various parameters on output quantities of interest can allow us to focus our future efforts in inverse uncertainty quantification
\\ \ \\
We will explore variance-based methods for global sensitivity analysis \footnotemark[1]
\end{block}
%\vspace{0.5cm}
%\begin{block}{Methods for Sensitivity Analysis}
%\begin{enumerate}[I]
%\item Derivative-based methods
%\item Regression-based methods
%\item \textcolor{red}{Variance-based methods}\footnotemark[1]
%\end{enumerate}
%\end{block}
\footnotetext[1]{Saltelli, Andrea, et al. ``Global sensitivity analysis: the primer''. John Wiley \& Sons, 2008.}
\end{frame}

%\subsection{Variance-Based Methods}
%\begin{frame}
%\frametitle{Global Sensitivity Analysis}
%\begin{block}{Variance-based methods}
%Variance-based methods are attractive for many reasons \footnotemark[1]
%\begin{enumerate}[I]
%\item They are model independent 
%\item They allow us to assess the relative importance of input factors and the effect of their interaction with other input factors
%\item Using variance as a metric preserves the uncertainty in the system (compared to mean-based approaches used in risk analysis)
%\end{enumerate}
%\vspace{0.5cm}
%The main pitfall associated with variance-based methods are the associated computational cost \footnotemark[1]
%\end{block}
%\footnotetext[1]{Saltelli, Andrea, et al. ``Global sensitivity analysis: the primer''. John Wiley \& Sons, 2008.}
%\end{frame}



\begin{frame}
\frametitle{Global Sensitivity Analysis}
\begin{block}{Variance-Based Methods}
For a generic model, Y is the output for a set of uncertain input factors $X = (X_1, X_2, \hdots, X_d)$

\begin{equation}
Y = f(x,t,X) 
\end{equation}\\

These variance-based methods are concerned with quantifying the sensitivity of the output to the uncertainty of the input using Sobol indices $S_i$ for input factors $X_i$
\begin{equation}
S_i = \frac{V_{X_i}(E_{X_{\sim i}}(Y \vert X_i))}{V(Y)} 
\end{equation}
\end{block}
\begin{block}{}
\begin{enumerate}[I]
\item Monte Carlo Sampling Methods \footnotemark[1]
\item \textcolor{red}{Polynomial Chaos Expansion (PCE)} \footnotemark[2]
\end{enumerate}
\end{block}
\footnotetext[1]{Sobol', Il'ya Meerovich. ``On sensitivity estimation for nonlinear mathematical models.'' Matematicheskoe modelirovanie 2.1 (1990): 112-118.}
\footnotetext[2]{Sudret, Bruno. ``Global sensitivity analysis using polynomial chaos expansions.'' Reliability Engineering \& System Safety 93.7 (2008): 964-979.}
\end{frame}


%\begin{frame}
%\frametitle{Global Sensitivity Analysis}
%\begin{block}{Variance-based methods}
%\begin{enumerate}[I]
%\item Monte Carlo Sampling Methods \footnotemark[1]
%\item Fourier Amplitude Sensitivity Test (FAST) and Random Balance Designs (RBD) \footnotemark[2]
%\item \textcolor{red}{Polynomial Chaos Expansion (PCE) Surrogate Modelling} \footnotemark[3]
%\end{enumerate}
%\end{block}
%\footnotetext[1]{Sobol', Il'ya Meerovich. ``On sensitivity estimation for nonlinear mathematical models.'' Matematicheskoe modelirovanie 2.1 (1990): 112-118.}
%\footnotetext[2]{Tarantola, Stefano, Debora Gatelli, and Thierry Alex Mara. ``Random balance designs for the estimation of first order global sensitivity indices.'' Reliability Engineering \& System Safety 91.6 (2006): 717-727.}
%\footnotetext[3]{Sudret, Bruno. ``Global sensitivity analysis using polynomial chaos expansions.'' Reliability Engineering \& System Safety 93.7 (2008): 964-979.}
%\end{frame}



\begin{frame}
\frametitle{Polynomial Chaos Surrogate}
\begin{block}{PC Representation of $f$}
Modelling the uncertain inputs, $X = (X_1, X_2, \hdots, X_d)$, as random variables, $\xi = (\xi_1, \hdots, \xi_d)$, we construct a PC representation of the output
\begin{equation}
f(x,t,X) \approx g(x,t,\xi) = \sum_{\alpha=0}^{P} y_{\alpha}(x,t)\Psi_\alpha(\xi)
\end{equation}

%The multivariate polynomial basis functions, $\Psi_\alpha(\xi) = \prod_{i =1}^{d} \psi_{\alpha_i}(\xi_i)$, are orthogonal with respect to the pdf, $p(\xi)$.\\
{\scriptsize 
$y_\alpha(X,t)$ are deterministic coefficients, $\Psi_\alpha(\xi) = \prod \psi_{\alpha_i}(\xi_i)$ are multivariate polynomials of random variables, and $\psi_{\alpha_i}(\xi_i)$ are univariate polynomials of random variables}
\vspace{0.25 cm}

Given that the polynomials $\Psi_\alpha$ are orthogonal with respect to $p(\xi)$, the Sobol indices can be obtained analytically as a ratio of the squares of PC coefficients \footnotemark[1]
\begin{equation}
S_i = \frac{\sum_{\alpha \in \mathcal{A}_i}y_{\alpha}^2 }{\sum_{\alpha = 1}^{P}y_{\alpha}^2}
\end{equation}
\end{block}
\footnotetext[1]{Sudret, Bruno. ``Global sensitivity analysis using polynomial chaos expansions.'' Reliability Engineering \& System Safety 93.7 (2008): 964-979.}
\end{frame}


\begin{frame}
\frametitle{Polynomial Chaos Surrogate}
\begin{example}
For a problem with 2 uncertain parameters, we have $Y = f(X_1,X_2) \approx g(\xi_1, \xi_2)$ 
\begin{equation*}
Y \approx y_0 + y_1\psi_1(\xi_1) + y_2\psi_1(\xi_2) + y_3\psi_2(\xi_1) + y_4\psi_1(\xi_1)\psi_1(\xi_2) + y_5\psi_2(\xi_2)
\end{equation*}

In total there are $2^d-1$ Sobol indices, hence for a case with 2 unknown input parameters ($d=2$) we have 3 indices, namely, $S_1$, $S_2$, and $S_{12}$\\ 
\vspace{0.2cm}
In general, we could more efficiently describe 100\% of the variance in the output using $2\times d$ quantities, namely the main and total effect terms, $S_1$, $S_2$, $S_{T_1}$, and $S_{T_2}$ 
\vspace{-0.2cm}
\begin{align*}
S_1 &= \frac{y_1^2 + y_3^2}{\sum_{\alpha = 1}^{5}y_{\alpha}^2} & S_2 &= \frac{y_2^2 + y_5^2}{\sum_{\alpha = 1}^{5}y_{\alpha}^2} 
\\ \ \\
S_{T_1} &= \frac{y_1^2 + y_3^2 + y_4^2}{\sum_{\alpha = 1}^{5}y_{\alpha}^2} & S_{T_2} &= \frac{y_2^2 + y_4^2 + y_5^2}{\sum_{\alpha = 1}^{5}y_{\alpha}^2}
\end{align*}
\end{example}
\end{frame}


%\begin{frame}
%\frametitle{Polynomial Chaos Surrogate}
%\begin{block}{PC Representation of $f$}
%For each \textit{design parameter}, $z_l$, $l = (1,2,\hdots,L)$, we will construct a Polynomial chaos representation of the output of function $f$
%\begin{equation}
%f(X,z_l) \approx \sum_{\alpha=0}^{A-1} y_{\alpha l}\Psi_\alpha(\xi)
%\end{equation}
%{\footnotesize
%$\alpha$ counts the multi-indices (or \textit{tuples}) $\alpha_1, \hdots, \alpha_d$, $\sum_{i = 1}^d \vert \alpha_i \vert \leq P$\\
%$A = (d+P)!/(d!P!)$\\
%$d$ is the dimensionality of the problem, ie. the number of random inputs $\xi = (\xi_1, \xi_2, \hdots, \xi_d)$\\
%$P$ is the total order of our PCE}\\
%\end{block}
%\end{frame}
%
%\begin{frame}
%\frametitle{Polynomial Chaos Surrogate}
%\begin{block}{Basis Functions, $\Psi_\alpha$}
%The multivariate polynomial basis function,s $\Psi_\alpha(\xi)$, are products of univariate polynomials $\psi_{\alpha_i}(\xi_i), i = (1,2,\hdots,d)$
%\begin{equation}
%\Psi_\alpha(\xi) = \prod_{i =1}^{d} \psi_{\alpha_i}(\xi_i)
%\end{equation}
%\end{block}
%\end{frame}


%\frametitle{Global Sensitivity Analysis}
%\begin{frame}
%\frametitle{Polynomial Chaos Surrogate}
%%\begin{columns}
%%\column{0.5\textwidth}
%\begin{block}{Basis Functions, $\Psi_\alpha$}
%\begin{table}
%\begin{tabular}{c | c | c || l}
%P & $\alpha$ & $(\alpha_1,\alpha_2,\alpha_3)$ & $\Psi_\alpha$ \\
%\hline 
%0 & 0 & (0,0,0) & $\Psi_0 = 1$ \\
%1 & 1 & (1,0,0) & $\Psi_1 = \psi_1(\xi_1)$ \\
%1 & 2 & (0,1,0) & $\Psi_2 = \psi_1(\xi_2)$ \\
%1 & 3 & (0,0,1) & $\Psi_3 = \psi_1(\xi_3)$ \\
%2 & 4 & (2,0,0) & $\Psi_4 = \psi_2(\xi_1)$ \\
%2 & 5 & (1,1,0) & $\Psi_5 = \psi_1(\xi_1)\psi_1(\xi_1)$ \\
%2 & 6 & (1,0,1) & $\Psi_6 = \psi_1(\xi_1)\psi_1(\xi_3)$ \\
%2 & 7 & (0,2,0) & $\Psi_7 = \psi_2(\xi_2)$ \\
%2 & 8 & (0,1,1) & $\Psi_8 = \psi_1(\xi_2)\psi_1(\xi_3)$ \\
%2 & 9 & (0,0,2) & $\Psi_9 = \psi_2(\xi_3)$ \\
%\end{tabular}
%\caption{Multi-indices and PC basis functions for $d=3$, $P=2$}
%\end{table}
%\end{block}
%\end{frame}


%\begin{frame}
%\frametitle{Polynomial Chaos Surrogate}
%\begin{block}{Basis Functions, $\Psi_\alpha$}
%The basis functions are orthonormal when weighted by their probability density function $\pi(\xi)$
%\begin{equation}
%\int_{\Omega} \Psi_{\alpha}(\xi) \Psi_{\beta}(\xi)\pi(\xi) = \delta_{\alpha\beta} \vert \vert \Psi_{\alpha l} \vert \vert ^2
%\end{equation}
%\end{block}
%\begin{block}{Statistical Moments}
%Leveraging the orthonormality of the basis functions and recalling $\Psi_0(\xi)=1$, the mean and variance are simply
%\begin{equation}
%\mathbb{E} \left[ Y \right] = \mathbb{E} \left[ \sum y_{\alpha l} \Psi_{\alpha}(\xi) \right] = y_{0l} \vert \vert \Psi_{0 l} \vert \vert 
%\end{equation}
%\begin{equation}
%Var \left[ Y \right] = \mathbb{E} \left[ \left(\sum y_{\alpha l} \Psi_{\alpha}(\xi) - y_{0l}\right)^2 \right] = \sum_{\alpha = 1}^{A-1}y_{\alpha l}^2 \vert \vert \Psi_{\alpha l} \vert \vert ^2
%\end{equation}
%\end{block}
%\end{frame} 

%\begin{frame}
%\frametitle{Polynomial Chaos Surrogate}
%\begin{block}{Polynomial Chaos Coefficients}
%The sensitivity indices are then simply calculated from the PCE coefficients
%\begin{equation}
%S_i(z_l) = \frac{\sum_{\alpha = \mathbb{I}_i}y_{\alpha l}^2 \vert \vert \Psi_{\alpha} \vert \vert ^2}{\sum_{\alpha = 1}^{A-1}y_{\alpha l}^2 \vert \vert \Psi_\alpha \vert \vert ^2}
%\end{equation}
%\begin{equation}
%{S_i(z_l)}^T = \frac{\sum_{\alpha = {\mathbb{I}_i}^T}y_{\alpha l}^2 \vert \vert \Psi_{\alpha} \vert \vert ^2}{\sum_{\alpha = 1}^{A-1}y_{\alpha l}^2 \vert \vert \Psi_\alpha \vert \vert ^2}
%\end{equation}
%{\footnotesize
%where $\mathbb{I}_i$ is the indices of basis terms that involve only the variable $\xi_i$,\\
% and ${\mathbb{I}_i}^T$ is the indices of all basis terms that involve the variable $\xi_i$}
%\end{block}
%\end{frame}

%\begin{frame}
%\frametitle{Global Sensitivity Analysis}
%\begin{block}{First Order Sobol Indices}
%\begin{equation}
%S_i = \frac{V_{X_i}(E_{X_{\sim i}}(Y \vert X_i))}{V(Y)}
%\end{equation}
%\end{block}
%\begin{block}{Total Effect Sobol Indices}
%\begin{equation}
%{S_i}^T = 1 - \frac{V_{X_{\sim i}}(E_{X_i}(Y \vert X_{\sim i}))}{V(Y)}
%\end{equation}
%\end{block}
%\end{frame}

\subsection{Polynomial Chaos Surrogate}
\frametitle{Global Sensitivity Analysis}
\begin{frame}
\frametitle{Polynomial Chaos Surrogate}
\begin{block}{Obtaining PC Coefficients}
\begin{enumerate}[I]
\item Evaluate the model at predetermined quadrature points
\item Quadrature method depends on the distribution of the random variable ($\xi$) and accordingly the family of orthogonal polynomials $\Psi_\alpha$
\end{enumerate}

%\begin{equation}
%y_{\alpha } = \displaystyle \int_\Omega f(X(\xi))\Psi_\alpha(\xi)\pi(\xi)d\xi\approx \sum_{q=1}^N w_q f(X(\xi^{(q)}))\Psi_\alpha(\xi)\pi(\xi^{(q)})
%\end{equation}
\end{block}
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/full_quad}
        \caption{\scriptsize{Full quadrature}}%
    \end{subfigure}
\quad
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/sparse_quad}
        \caption{\scriptsize{Sparse quadrature}}%
    \end{subfigure}
\caption{$3^{rd}$ order Legendre-Uniform quadrature points, weighted in 3-D}
\end{figure}
\end{frame}

\subsection{Results}
\begin{frame}
\frametitle{Results}
\begin{block}{Sensitivity Analysis of Pitch Parameters and Flutter}
\begin{enumerate}[I]
\item Looking at the flutter speed and associated flutter frequency for the underlying linearized aeroelastic system 
\item We create a surrogate model for these output quantities of interest as a function of uncertain parameters
\begin{enumerate}[$K_\theta$]
\item[$K_\theta$] Pitch stiffness
\item[$D_\theta$] Damping
\item[$I_\theta$] Moment of inertia
\end{enumerate}
\item Analysing the sensitivity indices for the uncertain parameters
\end{enumerate}
\end{block}
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/damping}
        %\caption{\scriptsize{Modal decay rate vs. airspeed}}
    \end{subfigure}
\quad
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/frequency}
        %\caption{\scriptsize{Modal frequencies vs. airspeed}}
    \end{subfigure}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Results}
\begin{columns}
\column{0.4\textwidth}
\begin{block}{Flutter Speed}
\begin{tabular}{c || l | l}
Parameter & \textcolor{blue}{$S_i$} & \textcolor{red}{${S_{T_i}}$} \\
\hline
$X_1 = I_\theta$ & $0.0823$ & $0.6491$\\
$X_2 = C_\theta$ & $0.0065$ & $0.0084$\\
$X_3 = K_\theta$ & $0.3444$ & $0.9094$\\
\end{tabular}
\end{block}
\begin{figure}
        \includegraphics[width=\textwidth]{figs/Si_FS}
    \end{figure}
\column{0.4\textwidth}
\begin{block}{Flutter Frequency}
\begin{tabular}{c || l | l}
Parameter & \textcolor{blue}{$S_i$} & \textcolor{red}{${S_{T_i}}$} \\
\hline
$X_1 = I_\theta$ & $0.3643$ & $0.3728$\\
$X_2 = C_\theta$ & $0.0012$ & $0.0076$\\
$X_3 = K_\theta$ & $0.6216$ & $0.6332$\\
\end{tabular}
\end{block}
\begin{figure}
        \includegraphics[width=\textwidth]{figs/Si_FF}
    \end{figure}
\end{columns}
\end{frame}



%\begin{frame}
%\frametitle{Results}
%\begin{block}{Time Dependent Sensitivity Analysis of Pitch Parameters}
%\begin{enumerate}[I]
%\item Assessing the influence of higher modes on the solution as the system parameters are varied\\
%\item For the response $w(x,t) \approx \sum_{i=1}^{M} W_i(t) \Phi_j(x)$, compare 
%\end{enumerate}
%
%\begin{equation}
%\frac{\lvert \lvert W_1(t) \rvert \rvert_2}{\sum_{i=1}^{M}\lvert \lvert W_i(t) \rvert \rvert _2}
%\end{equation}
%
%We assume a uniform distribution of our input factors
%\begin{enumerate}[$K_\theta$]
%\item[$K_\theta$] Pitch stiffness [0.5 1.0 ]Nm/rad
%\item[$D_\theta$] Damping [0 10]\% damping
%\item[$I_\theta$] Moment of inertia [1.0$\times 10^{-5}$ 1.0$\times 10^{-3}$ ]kgm$^2$
%\end{enumerate}
%\end{block}
%\end{frame}

\begin{frame}
\frametitle{Results}
\begin{block}{Response Surface of Flutter Speed}
\begin{enumerate}[I]
\item PCE response surface of the flutter speed as a function of $I_\theta$ and $K_\theta$ (for the mean value of $C_\theta$)
\item Samples taken along a grid for comparison
\end{enumerate}

\end{block}
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/PCE_MC_Flutter_speed}
        \caption{\scriptsize{Flutter Speed}}%
    \end{subfigure}
\quad
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/PCE_MC_Flutter_frequency}
        \caption{\scriptsize{Flutter Frequency}}%
    \end{subfigure}
\caption{Response surface taken at a mean damping value of 5\%}
\end{figure}
\end{frame}

\begin{frame}
%\frametitle{}
\begin{block}{Probability Density Functions}
We can perform very inexpensive Monte Carlo sampling of the polynomial chaos surrogate model to obtain pdfs of the quantities of interest
\end{block}
\begin{figure}
\includegraphics[width=\linewidth]{figs/PCE_MC_Sub}
%\caption{Schematic of the assembled wing apparatus}
\end{figure}
\end{frame}

\begin{frame}
%\frametitle{}
\begin{block}{Probability Density Functions}
Furthermore the same surrogate model can be sampled for different classes of input pdfs of the uncertain parameters (Rayleigh distribution shown)
\end{block}
\begin{figure}
\includegraphics[width=\linewidth]{figs/PCE_MC_Ray}
%\caption{Schematic of the assembled wing apparatus}
\end{figure}
\end{frame}

%\begin{frame}
%\frametitle{Polynomial Chaos Surrogate}
%%\begin{columns}
%%\column{0.5\textwidth}
%\begin{block}{Basis Functions, $\Psi_\alpha$}
%\begin{table}
%\begin{tabular}{c || l | l}
%Parameter & $S_i$ & ${S_i}^T$ \\
%\hline
%$M_\theta$ & $S_M = 0.1675$ & $S_M + S_{MC} + S_{MK} + S_{MCK}= 0.1866$\\
%$C_\theta$ & $S_D = 0.6298$ & $S_C + S_{MC} + S_{CK} + S_{MCK}=0.6630$\\
%$K_\theta$ & $S_K = 0.1672$ & $S_K + S_{MK} + S_{CK} + S_{MCK}=0.1863$\\
%\end{tabular}
%\caption{Multi-indices and PC basis functions for $d=3$, $P=2$}
%\end{table}
%\end{block}
%\end{frame}




\section{Summary}
\begin{frame}
\frametitle{Summary}
\begin{block}{}
\begin{enumerate}[I]
\item Derived a set of coupled nonlinear PDEs and an ODE that model the aeroelastic vibrations of the wing
\item Discretized the distributed parameter system using Galerkin's method to obtain a projection based reduced order model
\item Performed global sensitivity analyis using non-intrusive spectral projection PCE to study how the variation of the flutter speed and frequency can be apportioned to the input parameters
\end{enumerate}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Summary}
\begin{block}{Future Work}
\begin{enumerate}[I]
%\item Implementing nonlinear aerodynamic models for drag, lift and moment
\item Employ Proper Orthogonal Decomposition (POD) for further reduction of the dimension of the deterministic model
\item Apply GSA to study the effects of a broader spectrum of input parameters on quantities of interest relating to limit cycle oscillations of the nonlinear system
\item Perform full Bayesian analysis on the nonlinear aeroelastic system; assimilating wind tunnel data for state and parameter estimation, and model selection and validation
\end{enumerate}
\end{block}
\end{frame}

\begin{frame}{Acknowledgments}
\begin{block}{Financial contributions}
\begin{enumerate}[I]
\item Natural Sciences and Engineering Research Council of Canada
\item Ontario Student Assistance Program
\item Royal Military College of Canada
\end{enumerate}
\end{block}

\end{frame}

\end{document}


%blue
\begin{block}{Block Title}
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
\end{block}

%red
\begin{alertblock}{Block Title}
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
\end{alertblock}

%green
\begin{example}
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
\end{example}


\appendix
\backupbegin
...

\backupend
